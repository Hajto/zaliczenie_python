import curses

TEXT_WIDTH = 20
FRAME_HEIGHT = 6


def start_prompt(stdscr, text):
    stdscr.clear()
    curses.echo()
    height, width = stdscr.getmaxyx()

    frame_length = len(text) + 1 + TEXT_WIDTH
    start = int(width / 2 - frame_length / 2)
    height_start = int(height / 2 - FRAME_HEIGHT / 2)
    stdscr.addstr(height_start, start, "+" + frame_length * "-" + "+")
    stdscr.addstr(height_start + FRAME_HEIGHT, start, "+" + frame_length * "-" + "+")
    for i in range(1, FRAME_HEIGHT):
        stdscr.addch(height_start + i, start, "|")
        stdscr.addch(height_start + i, start + frame_length + 1, "|")

    stdscr.addstr(int(height_start + FRAME_HEIGHT / 2), start + 2, text + ":")

    cursor_x, cursor_y = stdscr.getyx()
    stdscr.move(cursor_x, cursor_y + 1)
    cursor_y += 1

    stdscr.refresh()

    buffer = ""
    key = stdscr.getkey()
    while key != "\n":
        buffer += key
        key = stdscr.getkey()

    curses.noecho()

    return buffer


def print_center(stdscr, text):
    stdscr.clear()
    height, width = stdscr.getmaxyx()
    width_center = int(width / 2)
    height_center = int(height / 2)
    stdscr.addstr(height_center, int(width_center - len(text) / 2), text)
    stdscr.refresh()
    stdscr.getkey()
