from curses import wrapper
from enum import Enum
from view import Prompt
from game import ClassicChessGame

CHEAT_SEQUENCE = "xyzzy"


class Keys(Enum):
    LEFT_ARROW = 260
    UP_ARROW = 259
    RIGHT_ARROW = 261
    DOWN_ARROW = 258
    SPACE = 32
    F = 102
    ENTER = 10


def main(stdscr):
    game = setup_game(stdscr)
    current_x = 0
    current_y = 0
    render_game(stdscr, game, current_y, current_x)

    status = ClassicChessGame.MoveResult.GAME_PASS

    cheat_code_counter = 0

    while status == ClassicChessGame.MoveResult.GAME_PASS:
        last_key = stdscr.getch()
        if last_key == Keys.LEFT_ARROW.value:
            # Left arrow
            if current_x > 0:
                current_x -= 1
        if last_key == Keys.RIGHT_ARROW.value:
            # Right arrow
            if current_x < game.board_options.size_x - 1:
                current_x += 1
        if last_key == Keys.UP_ARROW.value:
            if current_y > 0:
                current_y -= 1

        if last_key == Keys.DOWN_ARROW.value:
            if current_y < game.board_options.size_y - 1:
                current_y += 1

        if last_key == Keys.ENTER.value:
            break

        if last_key == Keys.F.value:
            status = game.make_move(ClassicChessGame.Move(ClassicChessGame.MoveType.FLAG, current_x, current_y))

        if last_key == Keys.SPACE.value:
            status = game.make_move(ClassicChessGame.Move(ClassicChessGame.MoveType.UNCOVER, current_x, current_y))
        elif chr(last_key) == CHEAT_SEQUENCE[cheat_code_counter]:
            cheat_code_counter += 1
            if cheat_code_counter == len(CHEAT_SEQUENCE):
                render_game(stdscr, game, current_y, current_x, True)
                stdscr.move(0, 0)
                stdscr.addstr("You filthy cheater!")
                stdscr.refresh()
                stdscr.getkey()
                cheat_code_counter = 0

        render_game(stdscr, game, current_y, current_x, debug=True)

    if status == ClassicChessGame.MoveResult.GAME_WON:
        Prompt.print_center(stdscr, "You won!")
    elif status == ClassicChessGame.MoveResult.GAME_OVER:
        Prompt.print_center(stdscr, "You lost!")


def render_game(stdscr, game, cursor_y, cursor_x, cheat = False, debug = False):
    stdscr.clear()

    if debug:
        stdscr.move(0,0)
        stdscr.addstr(f"Flags: {game.flags}, Correct-flags: {game.correct_flags}")

    FIELD_WIDTH = 3  # [{letter}] = 1 + 1 + 1
    board = game.board
    x_size = game.board_options.size_x
    y_size = game.board_options.size_y
    row_characters = x_size * FIELD_WIDTH

    w_height, w_width = stdscr.getmaxyx()

    screen_center_x = int(w_width / 2)
    screen_center_y = int(w_height / 2)

    top_edge = int(screen_center_y - y_size / 2)
    left_edge = int(screen_center_x - row_characters / 2)

    stdscr.addstr(top_edge - 1, left_edge - 1,
                  "+" + row_characters * "-" + "+")
    stdscr.addstr(top_edge + y_size, left_edge - 1,
                  "+" + row_characters * "-" + "+")
    for i in range(0, y_size):
        stdscr.addch(top_edge + i, left_edge - 1, "|")
        stdscr.addch(top_edge + i, left_edge + row_characters, "|")

    for i in range(0, y_size):
        stdscr.move(top_edge + i, left_edge)
        for j in range(x_size):
            stdscr.addstr("[" + field_to_str(board[j][i], cheat) + "]")

    stdscr.addstr(top_edge - 4, left_edge,
                  f"Bombs left: {game.board_options.bomb_count - game.flags - game.correct_flags}")
    stdscr.addstr(top_edge - 2, left_edge, f"Flags: {game.flags + game.correct_flags}")

    stdscr.move(top_edge + cursor_y, left_edge + cursor_x * 3 + 1)


def prompt_int(stdscr, title):
    try:
        return int(Prompt.start_prompt(stdscr, title))
    except ValueError:
        stdscr.clear()
        stdscr.addstr("Please enter integer value in correct bounds. Press enter to retry.")
        stdscr.refresh()
        stdscr.getkey()
        return prompt_int(stdscr, title)


def setup_game(stdscr):
    game_width = prompt_int(stdscr, "Szerokosc")
    game_height = prompt_int(stdscr, "Wysokosc")
    game_bomb_count = prompt_int(stdscr, "Ilosc Bomb")

    try:
        game = ClassicChessGame.ClassicChessGame(game_bomb_count, game_width, game_height)
        game.generate_board(ClassicChessGame.DefaultBoardGenerator())
        return game
    except ClassicChessGame.InvalidGameParameterException as error:
        stdscr.clear()
        stdscr.addstr(error.message + " Press enter to retry.")
        stdscr.refresh()
        stdscr.getkey()
        return setup_game(stdscr)


def test_keyboard(stdscr):
    stdscr.move(0, 0)
    last_key = stdscr.getch()
    stdscr.addstr(str(last_key))
    stdscr.refresh()
    stdscr.getkey()


def field_to_str(value, cheat):
    if value in [ClassicChessGame.FieldsEnum.FLAGGED_BOMB, ClassicChessGame.FieldsEnum.FLAG]:
        return "F"
    elif value == ClassicChessGame.FieldsEnum.EMPTY:
        return " "
    elif value in [ClassicChessGame.FieldsEnum.BOMB, ClassicChessGame.FieldsEnum.FLAGGED_BOMB ]and cheat:
        return "*"
    elif value == ClassicChessGame.FieldsEnum.BOMB:
        return " "
    elif value in [ClassicChessGame.FieldsEnum.MAYBE_FLAGGED_FALSE_BOMB, ClassicChessGame.FieldsEnum.MAYBE_FLAGGED_TRUE_BOMB]:
        return "?"
    else:
        return str(value)


wrapper(main)
