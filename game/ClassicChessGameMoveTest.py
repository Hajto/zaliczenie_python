import unittest
from GenericGame import FieldsEnum, Move, MoveType, MoveResult
from ClassicChessGame import ClassicChessGame

"""
Po kliknięciu lewym przyciskiem na pole:
    ○ Jeśli jest tam mina, wyświetlana jest wiadomość o przegranej i gra się
    kończy,
    ○ Jeśli w sąsiedztwie pola są miny, na przycisku wyświetlana jest ich liczba a
    pole dezaktywuje się,
    ○ W przeciwnym razie sąsiednie pola są sprawdzane tak jakby zostały kliknięte
    a pole dezaktywuje się.
"""


class ClassicChessGameMoveTest(unittest.TestCase):
    x = 4
    y = 4
    bombs = 3
    initial_board = [
        [FieldsEnum.EMPTY, FieldsEnum.EMPTY, FieldsEnum.EMPTY, FieldsEnum.EMPTY],
        [FieldsEnum.EMPTY, FieldsEnum.EMPTY, FieldsEnum.EMPTY, FieldsEnum.EMPTY],
        [FieldsEnum.EMPTY, FieldsEnum.EMPTY, FieldsEnum.BOMB, FieldsEnum.EMPTY],
        [FieldsEnum.BOMB, FieldsEnum.BOMB, FieldsEnum.EMPTY, FieldsEnum.EMPTY]
    ]

    def test_move_click_bomb(self):
        game = ClassicChessGame(self.bombs, self.x, self.y)
        game.board = [i.copy() for i in self.initial_board]
        self.assertEqual(game.make_move(Move(MoveType.UNCOVER, 3, 1)), MoveResult.GAME_OVER)

    def test_move_uncover(self):
        game = ClassicChessGame(self.bombs, self.x, self.y)
        game.board = [i.copy() for i in self.initial_board]
        self.assertEqual(game.make_move(Move(MoveType.UNCOVER, 2, 1)), MoveResult.GAME_PASS)
        self.assertEqual(
            game.board,
            [
                [0, 0, 0, 0],
                [0, 1, 1, 1],
                [2, 3, FieldsEnum.BOMB, FieldsEnum.EMPTY],
                [FieldsEnum.BOMB, FieldsEnum.BOMB, 2, FieldsEnum.EMPTY]
            ]
        )
        self.assertEqual(game.uncovered_fields, 11)

    def test_move_flag_false(self):
        game = ClassicChessGame(self.bombs, self.x, self.y)
        game.board = [i.copy() for i in self.initial_board]

        # Flag Empty field
        self.assertEqual(game.make_move(Move(MoveType.FLAG, 0, 0)), MoveResult.GAME_PASS)
        self.assertEqual(game.board[0][0], FieldsEnum.FLAG)
        self.assertEqual(game.correct_flags, 0)
        self.assertEqual(game.flags, 1)

        # Undo Flag
        self.assertEqual(game.make_move(Move(MoveType.FLAG, 0, 0)), MoveResult.GAME_PASS)
        self.assertEqual(game.board[0][0], FieldsEnum.EMPTY)
        self.assertEqual(game.correct_flags, 0)
        self.assertEqual(game.flags, 0)

    def test_move_flag_bomb(self):
        game = ClassicChessGame(self.bombs, self.x, self.y)
        game.board = [i.copy() for i in self.initial_board]

        # Flag Empty field
        self.assertEqual(game.make_move(Move(MoveType.FLAG, 3, 0)), MoveResult.GAME_PASS)
        self.assertEqual(game.board[3][0], FieldsEnum.FLAGGED_BOMB)
        self.assertEqual(game.correct_flags, 1)
        self.assertEqual(game.flags, 0)

        # Undo Flag
        self.assertEqual(game.make_move(Move(MoveType.FLAG, 3, 0)), MoveResult.GAME_PASS)
        self.assertEqual(game.board[3][0], FieldsEnum.EMPTY)
        self.assertEqual(game.correct_flags, 0)
        self.assertEqual(game.flags, 0)

    def test_click_win_flag_condition(self):
        game = ClassicChessGame(self.bombs, self.x, self.y)
        game.board = [i.copy() for i in self.initial_board]
        self.assertEqual(game.make_move(Move(MoveType.UNCOVER, 2, 1)), MoveResult.GAME_PASS)
        self.assertEqual(game.make_move(Move(MoveType.UNCOVER, 2, 3)), MoveResult.GAME_PASS)
        self.assertEqual(game.make_move(Move(MoveType.UNCOVER, 3, 3)), MoveResult.GAME_PASS)
        self.assertEqual(game.make_move(Move(MoveType.FLAG, 2, 2)), MoveResult.GAME_PASS)
        self.assertEqual(game.make_move(Move(MoveType.FLAG, 3, 0)), MoveResult.GAME_PASS)
        self.assertEqual(game.make_move(Move(MoveType.FLAG, 3, 1)), MoveResult.GAME_WON)

    def test_click_win_uncover_condition(self):
        game = ClassicChessGame(self.bombs, self.x, self.y)
        game.board = [i.copy() for i in self.initial_board]
        self.assertEqual(game.make_move(Move(MoveType.UNCOVER, 2, 1)), MoveResult.GAME_PASS)
        self.assertEqual(game.make_move(Move(MoveType.FLAG, 2, 2)), MoveResult.GAME_PASS)
        self.assertEqual(game.make_move(Move(MoveType.FLAG, 3, 0)), MoveResult.GAME_PASS)
        self.assertEqual(game.make_move(Move(MoveType.FLAG, 3, 1)), MoveResult.GAME_PASS)
        self.assertEqual(game.make_move(Move(MoveType.UNCOVER, 2, 3)), MoveResult.GAME_WON)
