from .GenericGame import *
from random import shuffle, randint


class InvalidGameParameterException(Exception):
    def __init__(self, *args, **kwargs):
        (field, expected, actual_value) = args
        self.message = str(field) + " should be " + str(expected) + " but actually was " + str(actual_value)
        Exception.__init__(self, self.message,
                           **kwargs)


class BoardOptions:
    def __init__(self, size_x, size_y, bomb_count):
        self.size_x = size_x
        self.size_y = size_y
        self.bomb_count = bomb_count
        self.validate()

    def validate(self):
        self.validate_size()
        self.validate_bombs()

    # Tu można na chama dorzucic lambdy
    def validate_size(self):
        if 3 > self.size_x or self.size_x > 15:
            raise InvalidGameParameterException("x", "in range of [3,15]", self.size_x)
        if 3 > self.size_y or self.size_y > 15:
            raise InvalidGameParameterException("y", "in range of [3,15]", self.size_y)

    def validate_bombs(self):
        if self.bomb_count <= 0:
            raise InvalidGameParameterException("Bombs", "more than 0", self.bomb_count)
        if (self.size_y * self.size_x) / 2 < self.bomb_count:
            raise InvalidGameParameterException("Bombs", "less than half of space", self.bomb_count)


class DefaultBoardGenerator:
    def __call__(self, board, options):
        bomb_counter = 0
        while bomb_counter < options.bomb_count:
            maybe_x = randint(0, options.size_x - 1)
            maybe_y = randint(0, options.size_y - 1)
            if board[maybe_x][maybe_y] == FieldsEnum.EMPTY:
                board[maybe_x][maybe_y] = FieldsEnum.BOMB
                bomb_counter += 1


class PairBoardGenerator:

    def __call__(self, board, options):
        for i in options.bomb_count:
            (x, y) = self.generate(options)
            board[x][y] = FieldsEnum.BOMB

    def generate(self, options):
        x = range(0, options.size_x)
        y = range(0, options.size_y)
        array = [(i, j) for i in x for j in y]
        shuffle(array)

        for i in array:
            yield i


class ClassicChessGame(GenericGame):
    # Board should be bidimensional array of Enums

    def __init__(self, bomb_count, size_x, size_y):
        self.board_options = BoardOptions(size_x, size_y, bomb_count)
        self.board = [[FieldsEnum.EMPTY for j in range(self.board_options.size_y)] for i in
                      range(self.board_options.size_x)]

        self.flags = 0
        self.correct_flags = 0
        self.uncovered_fields = 0

    def generate_board(self, generator):
        generator(self.board, self.board_options)

    def make_move(self, move):
        if move.move_type == MoveType.FLAG:
            self.set_flag(move.x, move.y)
            return self.apply_win_condition()
        elif move.move_type == MoveType.UNCOVER:
            return self.move_uncover(move)

    def win_condition(self):
        return self.uncovered_fields + self.correct_flags - self.flags == self.board_options.size_x * self.board_options.size_y

    def apply_win_condition(self):
        if self.win_condition():
            return MoveResult.GAME_WON
        else:
            return MoveResult.GAME_PASS

    def move_uncover(self, move):
        if self.board[move.x][move.y] == FieldsEnum.BOMB:
            return MoveResult.GAME_OVER
        elif self.board[move.x][move.y] == FieldsEnum.EMPTY:
            self.uncover_fields(move)
            return self.apply_win_condition()
        else:
            return MoveResult.GAME_PASS

    def uncover_fields(self, move):
        queue = [(move.x, move.y)]
        self.add_empty_neighbours(move.x, move.y, queue)

        while len(queue) > 0:
            (x, y) = queue.pop(0)
            if self.board[x][y] == FieldsEnum.EMPTY:
                returned = self.count_adjacent_mines(x, y)
                self.board[x][y] = returned
                self.uncovered_fields += 1
                if returned == 0:
                    self.add_empty_neighbours(x, y, queue)

    def reduce_neighbours(self, x, y, initial_value, reducer):
        count = initial_value
        max_x = range(0, self.board_options.size_x)
        max_y = range(0, self.board_options.size_y)

        def check(check_x, check_y):
            if check_x in max_x and check_y in max_y:
                reducer((check_x, check_y, self.board[check_x][check_y]), count)

        check(x - 1, y - 1)  # Top Left
        check(x, y - 1)  # Top Center
        check(x + 1, y - 1)  # Top Right

        check(x - 1, y)  # Middle Left
        check(x + 1, y)  # Middle Right

        check(x - 1, y + 1)  # Bottom Left
        check(x, y + 1)  # Bottom Center
        check(x + 1, y + 1)  # Bottom Right

        return count

    def count_adjacent_mines(self, x, y):
        return sum(
            self.reduce_neighbours(
                x,
                y,
                [0],
                lambda data, acc: acc.append(1) if data[2] in [FieldsEnum.BOMB, FieldsEnum.FLAGGED_BOMB] else None
            )
        )

    def add_empty_neighbours(self, x, y, queue):
        self.reduce_neighbours(
            x,
            y,
            [],
            lambda data, acc: queue.append((data[0], data[1])) if data[2] == FieldsEnum.EMPTY else None
        )

    def set_flag(self, x, y):
        # Set true flag
        if self.board[x][y] == FieldsEnum.BOMB:
            self.correct_flags += 1
            self.board[x][y] = FieldsEnum.FLAGGED_BOMB
        # Remove true flag
        elif self.board[x][y] == FieldsEnum.FLAGGED_BOMB:
            self.correct_flags -= 1
            self.flags += 1
            self.board[x][y] = FieldsEnum.MAYBE_FLAGGED_TRUE_BOMB
        # Set False positive flag
        elif self.board[x][y] == FieldsEnum.EMPTY:
            self.flags += 1
            self.board[x][y] = FieldsEnum.FLAG
        # Remove False positive flag
        elif self.board[x][y] == FieldsEnum.FLAG:
            self.board[x][y] = FieldsEnum.MAYBE_FLAGGED_FALSE_BOMB
        elif self.board[x][y] == FieldsEnum.MAYBE_FLAGGED_TRUE_BOMB:
            self.board[x][y] = FieldsEnum.BOMB
            self.flags -= 1
        elif self.board[x][y] == FieldsEnum.MAYBE_FLAGGED_FALSE_BOMB:
            self.flags -= 1
            self.board[x][y] = FieldsEnum.EMPTY
