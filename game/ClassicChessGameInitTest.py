import unittest
from ClassicChessGame import *

"""
Wprowadzenie mniejszego rozmiaru planszy niż 2x2 lub większego niż 15x15, liczby
min mniejszej niż 0 lub większej niż m*n powoduje wyświetlenie komunikatu o
błędzie. Nie można rozpocząć gry dopóki te parametry nie są poprawne. Walidacja
danych powinna wykorzystywać mechanizm wyjątków
"""

class ClassicChessGameInitTest(unittest.TestCase):
    def bomb_count(self, board, x, y):
        bombs = 0
        for i in range(x):
            for j in range(y):
                if board[i][j] == FieldsEnum.BOMB:
                    bombs += 1
        return bombs

    def test_board_generation(self):
        size_x = 5
        size_y = 5
        bombs = 3

        game = ClassicChessGame(bombs, size_x, size_y)
        game.generate_board(DefaultBoardGenerator())
        board = game.board
        assert len(board) == size_y
        for i in range(size_y):
            assert len(board[i]) == size_x

        assert self.bomb_count(board, size_x, size_y) == bombs

    def test_board_too_small(self):
        with self.assertRaises(InvalidGameParameterException):
            ClassicChessGame(2, 2, 2)
        with self.assertRaises(InvalidGameParameterException):
            ClassicChessGame(2, 10, 2)

    def test_board_too_big(self):
        with self.assertRaises(InvalidGameParameterException):
            ClassicChessGame(2, 20, 2)
        with self.assertRaises(InvalidGameParameterException):
            ClassicChessGame(2, 10, 20)

    def test_not_enough_bombs(self):
        with self.assertRaises(InvalidGameParameterException):
            ClassicChessGame(0, 10, 20)

    def test_too_many_bombs(self):
        with self.assertRaises(InvalidGameParameterException):
            ClassicChessGame(100, 10, 20)


if __name__ == '__main__':
    unittest.main()
