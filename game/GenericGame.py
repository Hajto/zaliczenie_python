from enum import Enum


class GenericGame:
    def make_move(self, move):
        raise NotImplemented

    def generate_board(self, generator):
        raise NotImplemented


class FieldsEnum(Enum):
    EMPTY = -1
    FLAG = -2
    BOMB = -3
    FLAGGED_BOMB = -4
    MAYBE_FLAGGED_TRUE_BOMB = -5
    MAYBE_FLAGGED_FALSE_BOMB = -6


class MoveType(Enum):
    FLAG = 0
    UNCOVER = 1


class MoveResult(Enum):
    GAME_OVER = 0
    GAME_WON = 1
    GAME_PASS = 2


class Move:
    def __init__(self, move_type, x, y):
        self.move_type = move_type
        self.x = x
        self.y = y

    def __str__(self):
        return str(self.move_type)+" on "+str(self.x)+":"+str(self.y)
